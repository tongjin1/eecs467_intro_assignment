#include <stdio.h>
#include <rc/encoder_eqep.h>
#include <rc/time.h>
#include <rc/motor.h>
int main(){
    rc_encoder_eqep_init();
	rc_motor_init();
	int enc_reading_1 = 0,enc_reading_2 = 0;
	const int enc_reading_1m_const = 6100;
	const int enc_reading_rotate = 850;
	for (int i =0; i<4; i++){
		rc_encoder_eqep_write(1,0);
		rc_encoder_eqep_write(2,0);
		rc_motor_set (1, -0.25);
		rc_motor_set (2, -0.28);
		enc_reading_1 = 0;
		enc_reading_2 = 0;
		while( enc_reading_1 < enc_reading_1m_const && enc_reading_2 < enc_reading_1m_const){
			enc_reading_1 = rc_encoder_eqep_read(1);
			enc_reading_2 = rc_encoder_eqep_read(2);
		}
		rc_motor_set (1, 0);
		rc_motor_set (2, 0);
		rc_encoder_eqep_write(1,0);
		rc_encoder_eqep_write(2,0);
		rc_motor_set (1, -0.25);
		rc_motor_set (2, 0.29);
		enc_reading_1 = 0;
		enc_reading_2 = 0;
		while( enc_reading_1 < enc_reading_rotate && enc_reading_2 > -enc_reading_rotate){
			enc_reading_1 = rc_encoder_eqep_read(1);
			enc_reading_2 = rc_encoder_eqep_read(2);
		}
		rc_motor_set (1, 0);
		rc_motor_set (2, 0);
	}
	rc_motor_cleanup();
    rc_encoder_eqep_cleanup();
    return 0;
}

